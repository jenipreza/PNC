<?php 
/*
  Plugin Name: Alerta Desaparecidos
  Plugin URI: 
  Description: Formulario en el que se ingresan los datos de una persona desaparecida
  Version: 1.1
  Author: DSerbino
  Author URI: 
 */

//Lanzador que inicia al activar el plugin
register_activation_hook( __FILE__, 'actualizar_crear_tabla' );

//Funcion que crea la tabla en la base de datos principal
function actualizar_crear_tabla(){
	global $wpdb;

	$nombre_tabla = $wpdb->prefix . 'desaparecidos';

	//sql con el statement de la tabla
    $sql = "CREATE TABLE $nombre_tabla (
      id int(11) NOT NULL AUTO_INCREMENT,
      nombres varchar(255) DEFAULT NULL,
      apellidos varchar(255) DEFAULT NULL,
      sexo varchar(255) DEFAULT NULL,
      edad varchar(255) DEFAULT NULL,
      caracteristicas varchar(255) DEFAULT NULL,
      hora time DEFAULT NULL,
      fecha date DEFAULT NULL,
      calle varchar(255) DEFAULT NULL,
      colonia varchar(255) DEFAULT NULL,
      ciudad varchar(255) DEFAULT NULL,
      departamento varchar(255) DEFAULT NULL,
      codigo varchar(255) DEFAULT NULL,
      telefono varchar(255) DEFAULT NULL,
      archivo varchar(255) DEFAULT NULL,
      UNIQUE KEY id (id)
    );";
 
 //upgrade contiene la función dbDelta la cuál revisará si existe la tabla o no
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
 
    //creamos la tabla
    dbDelta($sql);
}

//Lanzador que inicia al desactivar el plugin
register_deactivation_hook(__FILE__, 'remover_tabla' );

//Funcion que elimina la tabla de la base al desactivar el plugin
function remover_tabla(){
	//obtenemos el objeto $wpdb
    global $wpdb;

    //el nombre de la tabla, utilizamos el prefijo de wordpress
    $table_name = $wpdb->prefix . 'desaparecidos';

    //sql con el statement de la tabla
    $sql = "DROP table IF EXISTS $table_name";

	$wpdb->query($sql);
}

//Funcion que reenderiza el formulario
 function formulario_alerta($nombres, $apellidos, $sexo, $edad, $caracteristicas_fisicas, $hora, $fecha_ocurrido, $calle_numero, $colonia, $ciudad, $departamento, $codigo_postal, $profesion, $telefono, $archivo){

 	echo '
 	<div class="container" >
 		
	 			<form action="'.$_SERVER['REQUEST_URI'].'" method="post" accept-charset="utf-8" enctype="multipart/form-data" >

	 				<div class="row" >
						<div class="col-md-3">
							<label for="nombres">Nombres</label>
							<input class="form-control" type="text" name="nombres" value="' . ( isset( $_POST['nombres'] ) ? $nombres : null ) . '" >
						</div>
						<div class="col-md-3">
							<label for="apellidos">Apellidos</label>
							<input class="form-control" type="text" name="apellidos" value="' . ( isset( $_POST['apellidos'] ) ? $apellidos : null ) . '" >
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="sexo">Sexo</label><br>
								<input type="radio" name="sexo" value="Masculino"> Masculino<br>
								<input type="radio" name="sexo" value="Femenino"> Femenino
							</div>
							<div class="form-group">
								<label for="edad">Edad</label>
								<input class="form-control" type="text" name="edad" value="' . ( isset( $_POST['edad'] ) ? $edad : null ) . '" >
							</div>
							<div class="form-group">
								<label for="caracteristicas">Caracteristicas Fisicas</label>
								<textarea class="form-control" name="caracteristicas_fisicas" id="caracteristicas_fisicas" cols="30" rows="10"></textarea>
							</div>
							<div class="form-group">
								<label for="hora">Hora del hecho</label>
								<input class="form-control" type="time" name="hora">
							</div>
							<div class="form-group">
								<label for="fecha">Fecha del hecho</label>
								<input class="form-control" type="date" name="fecha_ocurrido">
							</div>
							<div class="row">
								<h6>Direccion completa en donde fue visto por ultima vez</h6>
								<div class="form-group">
									<label for="calle">Calle y numero</label>
									<input class="form-control" type="text" name="calle_numero">
								</div>
								<div class="form-group">
									<label for="colonia">Colonia</label>
									<input class="form-control" type="text" name="colonia">
								</div>
								<div class="form-group">
									<label for="ciudad">Ciudad</label>
									<input class="form-control" type="text" name="ciudad">
								</div>
								<div class="form-group">
									<label for="departamento">Departamento</label>
									<input class="form-control" type="text" name="departamento">
								</div>
								<div class="form-group">
									<label for="codigo_postal">Codigo Postal</label>
									<input class="form-control" type="text" name="codigo_postal">
								</div>
								<div class="form-group">
									<label for="profesion">Profesion</label>
									<input class="form-control" type="text" name="profesion">
								</div>
								<div class="form-group">
									<label for="telefono">Telefono de contacto</label>
									<input class="form-control" type="text" name="telefono">
								</div>
								<div class="form-group">
									<label for="archivo">Foto o video</label>
									<input class="form-control" type="text" name="archivo">
								</div>
							</div>
							
						</div>
					</div>

					<input class="btn btn-success" type="submit" name="submit" value="Guardar Informacion">

				</form>		
 	</div>	
 	';
 }

//funcion encargada de la validacion de los datos
function validacion_formulario($nombres, $apellidos, $sexo, $edad, $caracteristicas_fisicas, $hora, $fecha_ocurrido, $calle_numero, $colonia, $ciudad, $departamento, $codigo_postal, $profesion, $telefono, $archivo){

	global $alerta_errores;
	$alerta_errores = new WP_Error;

	if (empty($nombres) || empty($apellidos) || empty($sexo) || empty($edad) || empty($caracteristicas_fisicas) || empty($telefono)) {
		$alerta_errores->add('field', 'Hay campos que requieren su atencion');
	}

	if (is_wp_error( $alerta_errores )) {
		foreach ($alerta_errores->get_error_messages() as $error) {
			echo '<div>';
        	echo '<strong>ERROR</strong>:';
	        echo $error . '<br/>';
	        echo '</div>';
			}
	}

}

//funcion que realiza la insercion de datos en tabla respectiva
function guardar_alerta(){
	global $wpdb, $alerta_errores, $nombres, $apellidos, $sexo, $edad, $caracteristicas_fisicas, $hora, $fecha_ocurrido, $calle_numero, $colonia, $ciudad, $departamento, $codigo_postal, $profesion, $telefono, $archivo;

	if (1> count($alerta_errores->get_error_messages())) {
		$nombre_tabla = $wpdb->prefix . 'desaparecidos';

		$wpdb->insert(
			$nombre_tabla,
			array(
				'nombres' => $nombres,
			    'apellidos' => $apellidos,
			    'sexo' => $sexo,
			    'edad' => $edad,
			    'caracteristicas' => $caracteristicas_fisicas,
			    'hora' => $hora,
			    'fecha' => $fecha_ocurrido,
			    'calle' => $calle_numero,
			    'colonia' => $colonia,
			    'ciudad' => $ciudad,
			    'departamento' => $departamento,
			    'codigo' => $codigo_postal,
			    'telefono' => $telefono,
			    'archivo' => $archivo
			)
		);
	}
 }

//Funcion que realiza el procesamiento de la informacion capturada en el formulario
function alerta_procesado() {
    if ( isset($_POST['submit'] ) ) {
    	//Validacion del formulario
        validacion_formulario(
        $_POST['nombres'],
        $_POST['apellidos'],
        $_POST['sexo'],
        $_POST['edad'],
        $_POST['caracteristicas_fisicas'],
        $_POST['hora'],
        $_POST['fecha_ocurrido'],
        $_POST['calle_numero'],
        $_POST['colonia'],
        $_POST['ciudad'],
        $_POST['departamento'],
        $_POST['codigo_postal'],
        $_POST['profesion'],
        $_POST['telefono'],
        $_POST['archivo']
        );
         
        //limpiar data
        global $nombres, $apellidos, $sexo, $edad, $caracteristicas_fisicas, $hora, $fecha_ocurrido, $calle_numero, $colonia, $ciudad, $departamento, $codigo_postal, $profesion, $telefono, $archivo;

        $nombres = sanitize_text_field( $_POST['nombres'] );
        $apellidos = sanitize_text_field( $_POST['apellidos'] );
        $sexo = sanitize_text_field( $_POST['sexo'] );
        $edad = sanitize_text_field( $_POST['edad'] );
        $caracteristicas_fisicas        =   esc_textarea( $_POST['caracteristicas_fisicas'] );
        $calle_numero = sanitize_text_field( $_POST['calle_numero'] );
        $colonia = sanitize_text_field( $_POST['colonia'] );
        $ciudad = sanitize_text_field( $_POST['ciudad'] );
        $departamento = sanitize_text_field( $_POST['departamento'] );
        $telefono = sanitize_text_field( $_POST['telefono'] );
        $archivo = sanitize_text_field( $_POST['archivo'] );
        $codigo_postal = sanitize_text_field( $_POST['codigo_postal'] );
        $hora = sanitize_text_field( $_POST['hora'] );
        $fecha_ocurrido = sanitize_text_field( $_POST['fecha_ocurrido'] );





      
 		//Se guardaran los datos si y solo si no exiten problemas de validacion
        guardar_alerta(
        $nombres, 
        $apellidos, 
        $sexo, 
        $edad, 
        $caracteristicas_fisicas, 
        $hora, 
        $fecha_ocurrido, 
        $calle_numero, 
        $colonia, 
        $ciudad, 
        $departamento, 
        $codigo_postal, 
        $profesion, 
        $telefono, 
        $archivo
        );
    }
 
 	//Muestra del formulario en pantalla
    formulario_alerta(
        $nombres, 
        $apellidos, 
        $sexo, 
        $edad, 
        $caracteristicas_fisicas, 
        $hora, 
        $fecha_ocurrido, 
        $calle_numero, 
        $colonia, 
        $ciudad, 
        $departamento, 
        $codigo_postal, 
        $profesion, 
        $telefono, 
        $archivo
        );
}


//Creacion del shorcode [alerta_desaparecido]
add_shortcode( 'alerta_desaparecido', 'alerta_funcion' );

// funcion al que hace referencia al shotcode
function alerta_funcion(){
	ob_start();
	alerta_procesado();
	return ob_get_clean();
}

function formulario_busqueda($primer_nombre, $segundo_nombre, $apellidos, $departamento, $municipio, $numero_registro){
	echo '
	<div class="container-fluid" >
		<div class="col-lg-4">
			<div>
				<h3>Busqueda</h3>
				<p>Personas reportadas como desaparecidas</p>
			</div>

			<form action="'.$_SERVER['REQUEST_URI'].'" method="post" accept-charset="utf-8" enctype="multipart/form-data" >

				<div class="row" >
					<div class="col-lg-12" >
						<div class="form-group">
						<label for="nombres">Primer Nombre</label>
						<input class="form-control" type="text" name="primer_nombre" value="' . ( isset( $_POST['primer_nombre'] ) ? $primer_nombre : null ) . '" >
						</div>
						<div class="form-group">
							<label for="nombres">Segundo Nombre</label>
							<input class="form-control" type="text" name="segundo_nombre" value="' . ( isset( $_POST['segundo_nombre'] ) ? $segundo_nombre : null ) . '" >
						</div>
					</div>
					

				</div>

				<div class="row">
					<div class="col-lg-12">

						
							<div class="form-group">
								<label for="apellidos">Apellidos</label>
								<input class="form-control" type="text" name="apellidos" value="' . ( isset( $_POST['apellidos'] ) ? $apellidos : null ) . '" >
							</div>
							<div class="form-group">
								<label for="departamento">Departamento</label>
								<input class="form-control" type="text" name="departamento">
							</div>
							<div class="form-group">
								<label for="municipio">Municipio</label>
								<input class="form-control" type="text" name="municipio">
							</div>
							<div class="form-group">
								<label for="numero_registro">Numero Registro</label>
								<input class="form-control" type="text" name="numero_registro">
							</div>
						

					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
					<input class="btn btn-success btn-block" type="submit" name="submit" value="Buscar">
				</div>
				</div>
				

				

			</form>		

		</div>
		<div class="col-md-8">
			<div class="row" >
				<div class="col-lg-4" >
					<h3>Resultados</h3>
				</div>
				<br>
				<div class="col-lg-4" >
					<button class="btn btn-danger" >Imprimir Afiche</button>
				</div>
				<div class="col-lg-4" >
					<button class="btn btn-danger" >Ofrecer Pista</button>
				</div>
			</div>
			<br>
			<div class="row" >
				<div class="card">
					<div class="card-body">
						<div class="container-fluid">
							ACA VAN LAS FOTOSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
						</div>
						
					    
					 </div>
				</div>
			</div>
			<div class="row" >
				<div class="col-lg-6">
					<button class="btn btn-info btn-block" >Mas fotos</button>
				</div>
				<div class="col-lg-6">
					<button class="btn btn-info btn-block" >Video</button>
				</div>
			</div>
			<div class="row">
				<div class="container-fluid">
					ACA VA LA INFORMACION PERSONAL DEL DESAPARECIDO
				</div>
			</div>
			<div class="row" >
				<div class="col-lg-4" >
					<h3>Redes sociales</h3>
				</div>
				<br>
				<div class="col-lg-4" >
					<button class="btn btn-danger" >Imprimir Afiche</button>
				</div>
				<div class="col-lg-4">
					<button class="btn btn-danger" >Ofrecer Pista</button>
				</div>
			</div>
			
		</div>


	</div>	
	';
}


function busqueda_procesado(){

	formulario_busqueda(
		$primer_nombre, 
		$segundo_nombre, 
		$apellidos, 
		$departamento, 
		$municipio, 
		$numero_registro
	);
}

add_shortcode( 'busqueda_desaparecido', 'busqueda_funcion' );

function busqueda_funcion(){
	ob_start();
	busqueda_procesado();
	return ob_get_clean();
}