��    (      \  5   �      p     q  
   t       	   �  '   �     �     �  &   �     �  	                 1     @     E     S     _     m  
   z     �     �     �  
   �     �     �     �     �     �     �            -   1  &   _     �  &   �     �  )   �     �  +       0     9     ;     J     M  -   [     �     �  ,   �     �  	   �     �  8  	     <
     Q
     X
     j
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
               +     A     Y     k  ;   �  -   �     �  +        /  )   7     a  +   �           
       	                        (                $                              %   '      &      #             !                                    "                         + % comments ,  1 comment 1 comment on %2$s %1$s comments on %2$s By %s Category: %s Change title displayed above the menu. Comments are closed. Copyright DarkElements DarkElements is a free, clean, minimalistic, responsive, mobile-friendly WordPress theme. Features: sidebar, sidebar navigation, footer widgets, custom header image, custom background, custom logo to replace site title and tagline. For more info please check readme file. Enjoy. Default header Edit Featured post Footer Left Footer Middle Footer Right Format: %s Guido van der Leest Logo Menu Menu Title No comments Nothing Found Permalink to %s Posted on %s Primary Navigation Primary Sidebar Read More &raquo; Search Results for: %s Set a logo to replace site title and tagline. Sorry, no posts matched your criteria. Tag: %s This will overwrite the default title. Title You can add one or multiple widgets here. http://www.guidovanderleest.nl http://www.guidovanderleest.nl/darkelements PO-Revision-Date: 2017-10-30 11:38:52+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Themes - DarkElements
 + %s comentarios ,  1 comentarios 1 comentario en %2$s %1$s comentarios en %2$s Por %s Categoría: %s Cambia el título mostrado encima del menú. Los comentarios está cerrados. Copyright DarkElements DarkElements es un tema de WordPress gratuito, limpio, minimalista, adaptable, y preparado para dispositivos móviles. Funcionalidades: barra lateral, navegación en la barra lateral, widgets en el pie, imagen de cabecera personalizada, logo del sitio personalizable para reemplazar título del sitio y asunto.   Cabecera por defecto Editar Entrada destacada Zona de pie izquierda Pie centro Zona de pie derecha Formato: %s Guido van der Leest Logotipo Menú Título de Menú No hay comentarios Nada encontrado Enlaces permanentes a %s Publicado en %s Navegación principal Barra lateral principal Leer más &raquo; Resultados de búsqueda por: %s Fija un logo para reemplazar el título y asunto del sitio. Disculpa, no hay entradas para lo que buscas. Etiquetas: %s Esto sobrescribirá el título por defecto. Título Puede añadir uno o varios widgets aquí. http://www.guidovanderleest.nl http://www.guidovanderleest.nl/darkelements 