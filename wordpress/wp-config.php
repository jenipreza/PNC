<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'prueba');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'h{*+h$ck4kW.%K`R$TyOw%(~L^yaTXBe^A8s-ev00Ln(`[i5ko+|to1j?%;%Xcej');
define('SECURE_AUTH_KEY', '%<X9+g 2 Uu`ByW|C8j{xow)(vR /;iI`qYU@j(7>x]{ehBS=Dtuh(RJR2-y8k)a');
define('LOGGED_IN_KEY', '{h~@&EP;^QbQKU87csMtrNTr7gL!IE:2yjkMqS)QveBQ0R3Bo+yoLZCbiH`~0C}O');
define('NONCE_KEY', ';@E8)`]W4F>B3P#i[+xQiE[[{`0qneDIXG]HP^GHAsT`Dj={>aQh!62yr.LY5Ruq');
define('AUTH_SALT', 'Ch#x[<ph_<rezQkz4>^yQ-.;3y@8F/Q,T}`{M(2[SUp-&rat6?~MF%6e^}VFKaiR');
define('SECURE_AUTH_SALT', '~Pk;O+<1<>8M3G?IY!N_Pz*/a{z9R&L8q7g.;*C-e+@F,MU9J/*kSys-*_Pox*|Y');
define('LOGGED_IN_SALT', 'IPiB7[zjCqhup&O9/gh&D>Cjfs~rJN{{{<ev8tW.vykc/d`=5Px2=6GhQ4r{ul<k');
define('NONCE_SALT', 'xse(1L)Q0o<X&qNpn^Uf<~@2kI1Kg?gFQfP66uw)oh3Sa7c47{qxbC%*,)`x;aOZ');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'prueba';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

